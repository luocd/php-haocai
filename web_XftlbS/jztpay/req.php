﻿<?php
require_once 'inc.php';
$version='1.0';
$customerid=$userid;
$sdorderno=date("YmdHis").rand(1000,9999);
$total_fee=number_format($_REQUEST['amount'],2,'.','');
//$total_fee="0.01";

$code=$_REQUEST['bankid'];

if($code=="weixin" || $code=="alipay" || $code=="alipaywap" || $code=="gzhpay"){
	$paytype=$code;
	$bankcode="";
}
else{
	$paytype="bank";
	$bankcode=$code;
}


$notifyurl="http://".$_SERVER['HTTP_HOST']."/jztpay/notify.php";
$returnurl="http://".$_SERVER['HTTP_HOST']."/jztpay/return.php";
$remark=$_REQUEST["rechargeId"];
$get_code="";

$sign=md5('version='.$version.'&customerid='.$customerid.'&total_fee='.$total_fee.'&sdorderno='.$sdorderno.'&notifyurl='.$notifyurl.'&returnurl='.$returnurl.'&'.$userkey);

?>
<!doctype html>
<html>
<head>
    <meta charset="utf8">
    <title>正在转到付款页</title>
</head>
<body onLoad="document.pay.submit()">
    <form name="pay" action="http://www.xingjiepay.com/apisubmit" method="post">
        <input type="hidden" name="version" value="<?php echo $version?>">
        <input type="hidden" name="customerid" value="<?php echo $customerid?>">
        <input type="hidden" name="sdorderno" value="<?php echo $sdorderno?>">
        <input type="hidden" name="total_fee" value="<?php echo $total_fee?>">
        <input type="hidden" name="paytype" value="<?php echo $paytype?>">
        <input type="hidden" name="notifyurl" value="<?php echo $notifyurl?>">
        <input type="hidden" name="returnurl" value="<?php echo $returnurl?>">
        <input type="hidden" name="remark" value="<?php echo $remark?>">
        <input type="hidden" name="bankcode" value="<?php echo $bankcode?>">
        <input type="hidden" name="sign" value="<?php echo $sign?>">
        <input type="hidden" name="get_code" value="<?php echo $get_code?>">
    </form>
</body>
</html>

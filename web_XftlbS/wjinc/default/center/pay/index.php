
<!DOCTYPE html>
<html>
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Welcome</title>
<meta name="renderer" content="webkit" /> 
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="/static/css/style.css" rel="stylesheet" type="text/css" />
<link href="/static/css/skin.css" rel="stylesheet" type="text/css" />
<link href="/static/css/balls.css" rel="stylesheet" type="text/css" /> 
	<link href="/static/css/margin.css" rel="stylesheet" type="text/css"/>
	<link href="/static/css/center.css" rel="stylesheet" type="text/css"/>
</head>
<body class="iframe-body">
<script type="text/javascript" src="/static/lib/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/static/lib/jquery.cookie.js"></script>
<script type="text/javascript" src="/static/js/skin.js"></script>
<script type="text/javascript" src="/static/lib/util/common.js"></script>
<script type="text/javascript"> 
	var staticFileUrl = parent.getStaticDomain();
// 	var lunarDate = parent.lunarDate;
	var animalsYear = parent.animalsYear;
	var sysServerDate = parent.sysServerDate;
	var defaultSkin = parent.defaultSkin;
	var sysTrialGamePro = parent.sysTrialGamePro;
	var gameMap = parent.gameMap;
	var games = parent.games;
	var layer = parent.layer;
	parent.UserBet.playType = "NORMAL";
</script>
<script type="text/javascript"> 
$.ajaxSetup ({
	cache: false //close AJAX cache
});
</script>
<!--  error.js要放在最后面  -->
<script type="text/javascript" src="/static/lib/util/error.js" async="async"></script>

	<div style="height: 700px">
		<input id="pageType" type="hidden" value="PAY" />
		<input id="payVal" type="hidden" value="onlinePayment" />
		<div class="memberheader">
	<div class="useravatar floatleft">
		<img src="/static/images/skin/blue/userlogo.jpg" width="84" height="83" alt="">
	</div>
	<div class="memberinfo floatleft">
		<h1 class="floatleft">
			<script language="javaScript">
				now = new Date(), hour = now.getHours()
				if (hour < 6) {
					document.write("凌晨好")
				} else if (hour < 9) {
					document.write("早上好")
				} else if (hour < 12) {
					document.write("上午好")
				} else if (hour < 14) {
					document.write("中午好")
				} else if (hour < 17) {
					document.write("下午好")
				} else if (hour < 19) {
					document.write("傍晚好")
				} else if (hour < 22) {
					document.write("晚上好")
				} else {
					document.write("夜里好")
				}
			</script>
			, <span id="userName" class="blue"></span>
		</h1>
		<div class="balance floatleft">
			<div class="balancevalue floatleft">
				中心钱包 : <span class="blue"><span id="balance" class="balanceCount">0</span> 元</span>
			</div>
			<div
				class="floatright margintop7 marginright10 marginleft5 pointer">
				<a onclick="Center.init();" href="javascript:;">
					<img src="/static/images/skin/blue/btnrefresh.jpg" width="16" height="17" alt="">
				</a>
			</div>

		</div>
		<div class="gap5"></div>
		<p>欢迎来到<?=$this->iff($args[0], $args[0] . '－'). $this->settings['webName']?>,我们更专注彩票，博彩乐趣都在这里。</p>
		<p>最后登录：<span id="loginTime"></span></p>
	</div>
</div>
		
		<div class="membersubnavi">
			<div class="subnavi blue">
				充值
				<div class="subnaviarrow"></div>
			</div>
			<div class="subnavi blue">
				<a href="/center/withdraw/index.html">提款</a>
			</div>
			<div class="subnavi blue">
				<a href="/center/pay/list.html">充值记录</a>
			</div>
			<div class="subnavi blue">
				<a href="/center/withdraw/list.html">提款记录</a>
			</div>
		</div>
		<div class="steps">
			<div class="substeps">
				<div class="substepitmgray1 substepitmblue1">
					<b>01</b> 选择支付模式
				</div>
				<div class="substepitmgray2">
					<b>02</b> 填写金额
				</div>
				<div class="substepitmgray2">
					<b>03</b> 选择银行
				</div>
				<div class="substepitmgray2">
					<b>04</b> 充值到账
				</div>
			</div>
			<div class="line"></div>
			<div class="tabs">
				<div class="tabtitle">选择充值模式：</div>
				
								
                <?php if (isset($this->bankList['weixin'])) {?>
				<div class="tab hide" id="tp_wx" payval="weixin" onclick="Pay.initWeChat(this);" title="微信转账"><?=$this->bankList['weixin']['name']?></div> 
                <?php }?> 
				
			</div>
		</div>
<div class="subcontent" id="subpagetp_wx" style="display: block;">
			<div class="subrow">
				<div class="subcol1"></div>
				<div class="subcol2">
					<img src="/static/images/bank/method_wx.jpg" width="533" height="114" alt="">
				</div>
			</div>

			<div class="subrow">
				<div class="subcol1">支付帐户：</div>
				<div id="wx-subcol" class="subcol2" style="width: 120px"><div class="banklist marginbtm10">						<input type="radio" name="wxPayId" value="288" payee="好彩网" payee-name="好彩网" code="WEIXIN" pay-img="/upload/wx.png" class="bankradiobtn" bank-address="好彩网" checked="checked"> <i class="ico-weixin"></i>								<span style="padding-right: 8px;">微信</span>					</div></div>
				<span class="validation" style="display: block; padding-top: 10px;">
					<img src="/static/images/iconwarning.jpg" width="17" height="15" alt=""> <span class="smalltxt red">大额微信支付提示转账成功后,需提交存款信息</span>
				</span>
			</div>
			<div class="subrow">
				<div class="subcol1"></div>
				<div class="subcol2 smalltxt gray">
					温馨提示：为确保财务第一时间为您添加游戏额度，请您尽量不要转账整数（例如：欲入￥2000，请￥2000.68）谢谢！<br>
					<span class="blue">友情提示：请每次入款前登录会员核对银行账号是否使用。入款至已过期账号，公司无法查收，恕不负责！<br>在您转账成功后，请点击“我已存款”，通知客服确认到账哦！
					</span>
				</div>
			</div>
			<div class="subrow margintop30">
				<div class="subcol1"></div>
				<div class="subcol2">
					<input type="submit" class="c-button" value="开始充值" style="cursor:pointer" onclick="Pay.showPayWindow();">
				</div>
			</div>
		</div>
		</div>	
        
        </div>

	<script type="text/javascript" src="/static/lib/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">
var layer = parent.layer;
parent.UserBet.playType = "NORMAL";
</script>
<script type="text/javascript" src="/static/lib/util/md5.js"></script>
<script type="text/javascript" src="/static/lib/util/common.js"></script>
<script type="text/javascript" src="/static/lib/util/ctab.min.js"></script>
<!--  error.js要放在最后面  -->
<script type="text/javascript" src="/static/lib/util/error.js"></script>
	<script type="text/javascript" src="/static/js/center.js?v5"></script>
</body>
</html>
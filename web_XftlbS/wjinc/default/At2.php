<?php $this->display('head.php');?>
<div class="banner-about">
    <div class="radio">
    <div class="container">
        <span>最新消息 / NEWS</span>
        <marquee id="noticeDom" scrollamount="4" scrolldelay="100" direction="left" onmouseover="this.stop();" onmouseout="this.start();">全网第一最具公信力信誉平台！两面赔率1.993  定位赔率9.93  期期返水0.5%！集齐当红最热高频彩票一站式体验！玩法公平、规则公正、信誉公开！大额无忧！百万取款3分钟内火速到账！</marquee>
    </div>
</div>
</div>
<div class="main">
	<div class="max">
    <div class="container clearfix">
        <div class="menu">
            <ul>
                <li class="about">
                    <a href="At1.php">关于我们</a>
                </li>
                <li class="contact">
                    <a href="At9.php">联系我们</a>
                </li>
                <li class="partner">
                    <a href="At8.php">联盟合作</a>
                </li>
                <li class="deposit">
                    <a href="At3.php">存款帮助</a>
                </li>
                <li class="withdraw">
                    <a href="At7.php">提款帮助</a>
                </li>
                <li class="question">
                    <a href="At2.php">常见问题</a>
                </li>
            </ul>
        </div>
        <div class="content">
            <div class="text">
                <div class="tit">
                    常见问题 /QUESTIONS
                </div>
                <div>
                    <p class="cyl">◉一般常见问题</p>
                    <p class="cyl">Q1: 如何加入好彩网？</p>
                    <p>A1: 您可以直接点选 "立即开户"，确实填写资料后，可立即登记成为好彩网会员。</p>
                    <p class="cyl">Q2: 我可以直接在网络上存款提款吗？</p>
                    <p>A2: 可以，好彩网提供多种线上存款选择，详情请参照 "存款提款"</p>
                    <p class="cyl">Q3: 我在哪里可以找到游戏规则？</p>
                    <p>A3: 在游戏视窗中,右上角有"游戏规则"选项，让您在享受游戏乐趣的同时，清楚告诉您游戏的玩法、规则及派彩方式。</p>
                    <p class="cyl">Q4: 单注投注额最低是？</p>
                    <p>A4: 我们单注最低投注额为人民币1元.</p>
                    <p class="cyl">Q5: 最高投注额有限制吗？</p>
                    <p>A5: 任何一家正规博彩和网络博彩公司对客户的帐户投注都有单注和单注限额，没有限额的公司都基本属于没有任何风险控制的私人或骗子公司，今天的控制是为了明天能保证100%提款给您，这点您可以自行分析。每个项目都有不同的限额设定，详细请登陆会员都在「会员资料」页面查看。</p>
                    <p class="cyl">Q6: 我帐户里面的帐怎麽结算？</p>
                    <p>A6: 请到登入会员，会员页面查看，「今天已结」按天显示每天的结算结果，点击进入查看今天所下注每一笔情况。</p>
                    <p class="cyl">Q7: 如果忘记密码怎麽办？</p>
                    <p>A7: 联系24小时线上客服人员谘询协助取回你的帐号密码。</p>
                    <p class="cyl">Q8: 当忘记提款密码时怎麽办？</p>
                    <p>A8: 你可通过24小时线上客服人员协助处理。</p>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<?php $this->display('foot.php');?>
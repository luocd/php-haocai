<?php $this->display('head.php');?>
<link rel="stylesheet" href="jh/css/login.css">
    <div class="bannerpromo">
		<div class="g_w1">
			<img src="jh/images/banner_login.jpg" width="1000" height="221" alt="" />
			<div class="headertxt">注册</div>
			<div class="headersubtxt">欢迎来到迪士尼彩乐园，我们更专注彩票，博彩乐趣都在这里。</div>
		</div>
	</div>

	<div class="loginpanel">
		<div class="g_w1 clearfix body_bg">
			<div class="stepitm stepactive" style="z-index: 3;">
				<div class="stepno">01</div>
				<div class="stepnotxt">设置账户及登录密码</div>
				<div class="hlfcircle hlfcircleactive"></div>
			</div>
			<div class="stepitm" style="z-index: 2;">
				<div class="stepno ml90">02</div>
				<div class="stepnotxt">注册成功</div>
				<div class="hlfcircle"></div>
			</div>
			<div class="stepitm" style="z-index: 1;">
				<div class="stepno ml90">03</div>
				<div class="stepnotxt">登录网站</div>
			</div>

		</div>

		<div class="g_w1 clearfix body_bg">
		<form action="user/registers" method="post">
			<div class="row mt35 clearfix">
				<div class="col1">登录账号:</div>
				<div class="col2">
					<input type="text" maxlength="15" name="username"  class="textbox1">
				</div>
				<div id="nameTips" class="col3">＊帐户名由4-10个字符组成</div>
			</div>
			<div class="row  clearfix">
				<div class="col1">登录密码:</div>
				<div class="col2">
					<input type="password" maxlength="40" name="password" class="textbox1 passwd">
				</div>
				<div class="col3">*格式长度6-15个字符内,请牢记</div>
			</div>
			<div class="row  clearfix">
				<div class="col1">确认密码:</div>
				<div class="col2">
					<input type="password" maxlength="40" name="cpasswd" class="textbox1 chpasswd">
				</div>
				<div class="col3">*格式长度6-15个字符内,请牢记</div>
			</div>
			<div class="row clearfix">
				<div class="col1">真实姓名:</div>
				<div class="col2">
					 <input type="text" name="name" class="textbox1">
				</div>
				<div class="col3"></div>
			</div>
			<div class="row clearfix">
				<div class="col1">取款密码:</div>
				<div class="col2">
					 <input type="text" name="coinpwd" class="textbox1 qcoinpwd">
				</div>
				<div class="col3"></div>
			</div>
			<div class="row  clearfix">
				<div class="col1">电子邮箱:</div>
				<div class="col2">
					<input name="email" type="text" size="40" maxlength="30" class="textbox1">
	
				</div>
				<div class="col3">*电子邮箱(Email)格式：88888888@qq.com</div>
			</div>
			
			<div class="row" style="margin-bottom: 50px;">
				<div class="col1"></div>
				<div class="col2">
					<input  value="提交注册" type="submit" class="submitbtn" onclick="return check(form)">
				</div>
			</div>
			</form>
			
			
			

		</div>
	</div>
<script>
$('.chpasswd').blur(function(){
	var  chpasswd = $('.chpasswd').val();
	var  passwd= $('.passwd').val();
	if(chpasswd!=passwd){
		alert("两次输入密码不一样");
		return false;
	}
	

})
$('.qcoinpwd').blur(function(){
	var  qcoinpwd = $('.qcoinpwd').val();
	var  passwd= $('.passwd').val();
	if(qcoinpwd==passwd){
		alert("登陆密码和取款密码不能相同");
		return false;
	}
	

})
function check(form){
	 if(form.username.value==""){
        alert("帐号不能为空");
        form.username.focus();
        return false;
    }
	if(form.password.value==""){
        alert("密码不能为空");
        form.password.focus();
        return false;
    }
	if(form.name.value==""){
        alert("真实姓名不能为空");
        form.name.focus();
        return false;
    }
	if(form.coinpwd.value==""){
        alert("取款密码不能为空");
        form.coinpwd.focus();
        return false;
    }
    form.submit();
}












</script>
<?php $this->display('foot.php');?>
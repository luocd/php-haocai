<?php $this->display('head.php');?>
<div class="banner-about">
    <div class="radio">
    <div class="container">
        <span>最新消息 / NEWS</span>
        <marquee id="noticeDom" scrollamount="4" scrolldelay="100" direction="left" onmouseover="this.stop();" onmouseout="this.start();">全网第一最具公信力信誉平台！两面赔率1.993  定位赔率9.93  期期返水0.5%！集齐当红最热高频彩票一站式体验！玩法公平、规则公正、信誉公开！大额无忧！百万取款3分钟内火速到账！</marquee>
    </div>
</div>
</div>
<div class="main">
	<div class="max">	
    <div class="container clearfix">
       <div class="menu">
            <ul>
                <li class="about">
                    <a href="At1.php">关于我们</a>
                </li>
                <li class="contact">
                    <a href="At9.php">联系我们</a>
                </li>
                <li class="partner">
                    <a href="At8.php">联盟合作</a>
                </li>
                <li class="deposit">
                    <a href="At3.php">存款帮助</a>
                </li>
                <li class="withdraw">
                    <a href="At7.php">提款帮助</a>
                </li>
                <li class="question">
                    <a href="At2.php">常见问题</a>
                </li>
            </ul>
        </div>
        <div class="content">
            <div class="text">
                <div class="tit">
                    关于我们 / ABOUT US
                </div>
                <div>
                    <p>好彩网于2011年成立，专业经营各项博彩业务，现已推出高频彩票现金投注网，主营北京赛车PK10、幸运飞艇、PC蛋蛋、重庆时时彩、天津时时彩、新疆时时彩、广东快乐十分、湖南快乐十分、江苏快三、香港六合彩等项目，完全自助注册开户，现金开户， 现金投注。我们拥有稳定的平台，成熟的玩法，简单的下注流程、以及优质的客户服务。</p>
                    <p class="mgb10"></p>
                    <p class="cyl mgb10">我们的承诺</p>
                    <p class="cyl">1.高效存取款 </p>
                    <p class="mgb10">好彩网一直注重用户体验，高效快速的存取款业务是用户最实在的体验，所以我们一直致力于开发最新的收支业务和人员培训保证高效的存取款，保证3分钟存提款到账，为你快速享受游戏乐趣提供最大保证。</p>
                    <p class="cyl">2.精英专业团队</p>
                    <p class="mgb10">好彩网客服均经过严格的筛选及专业的培训，用心打造精英客服团队为玩家提供优质的服务质量。全天候24小时为您提供专业、贴心的VIP服务，让您有宾至如归的感觉。</p>
                    <p class="cyl">3.信息安全隐私</p>
                    <p class="mgb10">提供平稳的加密安全通道128位SSL加密标准)，服务器的镜像备份保证了数据库和客户信息的安全。玩家网上支付和银行交易由国际金融机构在高标准的安全和机密的网络中进行。另外，客户在本平台的所有活动均严格保密，我们不会向任何第三方包括政府透露客户任何的个人信息及资料。</p>
                    <p class="cyl">4.负责任博彩</p>
                    <p class="mgb10">好彩网承诺提供「负责任博彩」，并确保每位客户能与我们一起享受到博彩的乐趣。但是我们也了解到有小部分的人有时候未必能控制自己的投注行为，如遇相关问题， 我们会积极鼓励这些用户及时与我们联络，以便尽快提供相关帮助。</p>
                    <p class="cyl">5.公平诚信</p>
                    <p class="mgb10">好彩网作为国际知名的线上博彩营运商，对接入的所有平台均经过严格的筛选，只选用国际知名线上博彩平台，保证让每位玩家在一个公平、公正的环境下进行游戏。同时菲律宾政府First Cagayan leisure and Resort Corporation会对游戏平台的数据进行监控最终确保游戏的公平性和真实性。</p>
                    <p class="cyl mgb10">选择好彩网，十大放心理由</p>
                    <p class="cyl">一、平台足够权威</p>
                    <p>好彩网获得GEOTRUST国际认证，确保网站公平、公正、公开。</p>
                    <p class="cyl">二、排名足够靠前</p>
                    <p>好彩网为澳门最具公信力博彩品牌，信誉驰名亚洲，在全球海量博彩公司中独占前茅的博彩集团。</p>
                    <p class="cyl">三、资金足够安全</p>
                    <p>好彩网雄厚的资金链信誉良好，大额存款无忧，资金安全有保障，免去您一切后顾之忧</p>
                    <p class="cyl">四、取款足够自由</p>
                    <p>好彩网早上9点到凌晨2点自由存取款，取款3-5分钟火速到帐，且终身免手续费。</p>
                    <p class="cyl">五、活力足够给力</p>
                    <p>好彩网时时推出各项给力活动，优惠送不停，让您真正赚翻天。</p>
                    <p class="cyl">六、安全足够给力</p>
                    <p>好彩网采用128位SSL加密技术和严格的安全管理体系，确保客户资料安全得到最完善的保障。</p>
                    <p class="cyl">七、服务足够贴心</p>
                    <p>好彩网7X18小时客服贴心的服务，温馨的呵护每一位玩家。</p>
                    <p class="cyl">八、营业足够合法</p>
                    <p>好彩网是首家获澳门特别行政区政府发出博彩经营权的企业，首间上市的持牌博彩经营商。</p>
                    <p class="cyl">九、操作足够简单</p>
                    <p>好彩网秉承以客户为中心的宗旨，在开发之初就设计了诸多人性化的功能和特性，省去繁琐的麻烦，让您轻松操作。</p>
                    <p class="cyl">十、VIP足够优势</p>
                    <p>好彩网VIP尊享贵宾级的专属服务让您享有宾至如归般的帝王享受。</p>
                    <p class="cyl">选择好彩网，等于为自己选择一份线上品牌信誉的保障！！！</p>
                </div>
            </div>
        </div>
       </div>
    </div>
</div>
<?php $this->display('foot.php');?>
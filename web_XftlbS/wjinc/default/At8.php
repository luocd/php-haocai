<?php $this->display('head.php');?>
<div class="banner-reg">
    <div class="radio">
    <div class="container">
        <span>最新消息 / NEWS</span>
        <marquee id="noticeDom" scrollamount="4" scrolldelay="100" direction="left" onmouseover="this.stop();" onmouseout="this.start();">123456</marquee>
    </div>
</div>
</div>
<div class="main">
	<div class="max">
    <div class="container clearfix">
        <div class="menu">
            <ul>
                <li class="about">
                    <a href="At1.php">关于我们</a>
                </li>
                <li class="contact">
                    <a href="At9.php">联系我们</a>
                </li>
                <li class="partner">
                    <a href="At8.php">联盟合作</a>
                </li>
                <li class="deposit">
                    <a href="At3.php">存款帮助</a>
                </li>
                <li class="withdraw">
                    <a href="At7.php">提款帮助</a>
                </li>
                <li class="question">
                    <a href="At2.php">常见问题</a>
                </li>
            </ul>
        </div>
        <div class="content">
            <div class="text">
                <div class="links">
                    <ul class="clearfix">
                        <li><a href="At8.php">合作方案</a></li>
                        <li><a href="At4.php">联盟协议</a></li>
                        <li><a href="At5.php">代理注册</a></li>
                        <li><a href="/">代理登陆</a></li>
                    </ul>
                </div>
                <div>
				<p>加盟方案<br>
   好彩网拥有多元化的产品，使用最公平、公正、公开的系统，在市场上的众多网站中，我们自豪的提供会员最优惠的回馈，给予合作伙伴最优势的营利回报! 无论 拥有的是网络资源，或是人脉资源，都欢迎您来加入好彩网合作伙伴的行列，无须负担任何费用，就可以开始无上限的收入。 选择好彩网，绝对是您最聪明的选择!</p>
<p>代理条件：<br>
 a.具有便利的计算机上网设备。
 <br>
b.有一定的人脉资源、网络资源或自己的广告网站。<br>

c.每期都要有5个实际有效投注的会员以上，如达不到就累积到下期计算佣金！<br>
<p>有效会员：<br>
月结区间内进行过最少五次有效下注且投注总额不低于1000RMB的会员！ 如联盟体系当月未达﹛月最低有效会员﹜最低门坎5人，则该月无法领取佣金回馈。联盟体系当月营利达到标准，而﹛月最低有效会员﹜人数未达相应最低门坎，则该月佣金累积到下期计算退佣。<br>
<p>代理收入：<br>
A：比如您本月的代理账号内有赢利的情况下，就可拥有以下收入:<br>
1.一个月内公司在您的代理账号内纯赢利达到1000元-50000元，可享受其中15%的佣金。<br>
2.一个月内公司在您的代理账号内纯赢利达到50001元-100000元，可享受其中20%的佣金。<br>
3.一个月内公司在您的代理账号内纯赢利达到100001元-200000元，可享受其中25%的佣金。<br>
4.一个月内公司在您的代理账号内纯赢利达到200001元-500000元，可享受其中30%的佣金。<br>
5.一个月内公司在您的代理账号内纯赢利达到500001元以上，可享受其中35%的佣金。<br>
＊每位加入合营商的客户，如果您在第一个月的有效会员未达到公司要求，但有产生佣金，您的佣金将累计到下个月。<br>
*注:禁止代理商私自开设会员帐号进行非法投注套利。任何使用不诚实方法骗取代理佣金或下线会员与代理商同IP的我们视为代理商自己开设会员游戏，将取消代理资格并永久冻结账户，佣金一律不予发放。同IP出现多个会员的话，将被视为无效会员，不得计算在内。<br>
  <br>
  <br>
</p>
						</div>
                </div>
            </div>
        </div>
        
        
     </div>   
    </div>
</div>
<?php $this->display('foot.php');?>
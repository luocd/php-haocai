
<!DOCTYPE html>
<html>
<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>Welcome</title>
<meta name="renderer" content="webkit" /> 
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="/static/css/style.css" rel="stylesheet" type="text/css" />
<link href="/static/css/skin.css" rel="stylesheet" type="text/css" />
<link href="/static/css/balls.css" rel="stylesheet" type="text/css" /> 
</head>
<body class="iframe-body">
<script type="text/javascript" src="/static/lib/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/static/lib/jquery.cookie.js"></script>
<script type="text/javascript" src="/static/js/skin.js"></script>
<script type="text/javascript" src="/static/js/Tdrag.min.js"></script>
<script type="text/javascript" src="/static/lib/util/common.js"></script>
<script type="text/javascript"> 
	var staticFileUrl = parent.getStaticDomain();
// 	var lunarDate = parent.lunarDate;
	var animalsYear = parent.animalsYear;
	var sysServerDate = parent.sysServerDate;
	var defaultSkin = parent.defaultSkin;
	var sysTrialGamePro = parent.sysTrialGamePro;
	var gameMap = parent.gameMap;
	var games = parent.games;
	var layer = parent.layer;
	parent.UserBet.playType = "NORMAL";
</script>
<script type="text/javascript"> 
$.ajaxSetup ({
	cache: false //close AJAX cache
});
</script>
<!--  error.js要放在最后面  -->
<script type="text/javascript" src="/static/lib/util/error.js" async="async"></script>

<!--main-->
<div class="main clearfix">
	<div class="content">
		<div class="sub-wrap clearfix">
			<!--cont-main-->
			<div class="cont-main">
			
			
			
<div id="subpage"> 
   <div class="cont-col3"> 
                   <table class="u-table2 play_tab_1">
			      <thead> 
			        <th align="left" style="padding-left: 20px;border-right: none;">
			        	<span id="page_game_name"></span>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="page_name">两面盘</span>
			        	<span>
			        	&nbsp;&nbsp
			        	当前彩种输赢：
			        	</span>
			        	<span id="total_sum_money" style="color: red;">0</span>
			        </th>
			        <th align="right" style="padding-right:20px; color:#626262; font-size:14px;font-weight:normal; border-left: none;">
			        	<span class="c-txt2" id="next_turn_num"></span>&nbsp;期
			        	距离封盘：<span class="c-txt1" id="bet-date">00:00</span>
			        	距离开奖：<span class="c-txt1" id="open-date">00:00</span>
			        </th>  
			      </thead> 
			     </table> 
    	<div class="cont-col3-hd clearfix"> 
     <!-- 
        <div class="cont-col3-box1">
         	   投注类型：
            <a href="#" class="cur">快捷</a>
            <a href="#">一般</a>
        </div>
     --> 
     <div class="cont-col3-box2"> 
		
<!--       <label> <input type="checkbox" class="u-cb1" /> 预设 </label>  -->
      	金额 
      <input id="bet_money1" type="text" class="bet-money" /> 
      <a href="javascript:parent.UserBet.openBetWindow()" id="openBetWinBtn1" class="u-btn1">确定</a> 
      <a href="javascript:parent.UserBet.resetData()" class="u-btn1">重置</a>
		<span onclick="jgzss(72)" style="float:right;display:block;background: linear-gradient(to bottom, #ff925f 0%,#ff7340 100%);color: #FFFFFF;cursor: pointer; border: 1px solid #ff6835; border-radius: 3px;margin-top:3px">结果走势</span>
     </div> 
    </div> 
     
    <div class="cont-col3-bd" id="bet_tab"> 
     <ul class="cont-list1 clearfix"> 
      <li> 
       <table class="u-table2 play_tab_11"> 
        <thead> 
         <tr> 
          <th colspan="3">冠军</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511107" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511107" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511107" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511108" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511108" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511108" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511109" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511109" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511109" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511110" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511110" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511110" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511111" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511111" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511111" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511112" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511112" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511112" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511113" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511113" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511113" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511114" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511114" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511114" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511115" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511115" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511115" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511116" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511116" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511116" class="amount"><input type="text" /></td>
         </tr> 
        </tbody> 
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_12"> 
        <thead> 
         <tr> 
          <th colspan="3">亚军</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511207" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511207" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511207" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511208" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511208" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511208" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511209" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511209" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511209" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511210" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511210" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511210" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511211" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511211" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511211" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511212" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511212" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511212" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511213" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511213" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511213" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511214" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511214" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511214" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511215" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511215" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511215" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511216" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511216" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511216" class="amount"><input type="text" /></td>
         </tr> 
        </tbody> 
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_13"> 
        <thead> 
         <tr> 
          <th colspan="3">第三名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511307" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511307" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511307" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511308" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511308" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511308" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511309" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511309" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511309" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511310" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511310" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511310" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511311" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511311" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511311" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511312" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511312" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511312" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511313" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511313" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511313" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511314" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511314" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511314" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511315" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511315" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511315" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511316" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511316" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511316" class="amount"><input type="text" /></td>
         </tr> 
        </tbody>
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_14"> 
        <thead> 
         <tr> 
          <th colspan="3">第四名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511407" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511407" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511407" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511408" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511408" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511408" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511409" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511409" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511409" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511410" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511410" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511410" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511411" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511411" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511411" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511412" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511412" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511412" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511413" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511413" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511413" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511414" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511414" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511414" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511415" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511415" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511415" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511416" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511416" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511416" class="amount"><input type="text" /></td>
         </tr> 
        </tbody>
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_15"> 
        <thead> 
         <tr> 
          <th colspan="3">第五名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511507" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511507" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511507" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511508" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511508" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511508" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511509" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511509" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511509" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511510" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511510" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511510" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511511" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511511" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511511" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511512" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511512" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511512" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511513" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511513" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511513" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511514" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511514" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511514" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511515" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511515" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511515" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511516" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511516" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511516" class="amount"><input type="text" /></td>
         </tr>  
        </tbody> 
       </table> </li> 
     </ul>
      
      
      <ul class="cont-list1 clearfix"> 
      <li> 
       <table class="u-table2 play_tab_16"> 
        <thead> 
         <tr> 
          <th colspan="3">第六名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511607" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511607" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511607" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511608" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511608" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511608" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511609" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511609" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511609" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511610" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511610" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511610" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511611" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511611" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511611" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511612" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511612" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511612" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511613" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511613" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511613" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511614" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511614" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511614" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511615" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511615" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511615" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511616" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511616" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511616" class="amount"><input type="text" /></td>
         </tr>   
        </tbody> 
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_17"> 
        <thead> 
         <tr> 
          <th colspan="3">第七名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511707" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511707" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511707" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511708" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511708" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511708" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511709" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511709" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511709" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511710" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511710" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511710" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511711" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511711" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511711" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511712" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511712" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511712" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511713" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511713" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511713" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511714" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511714" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511714" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511715" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511715" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511715" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511716" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511716" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511716" class="amount"><input type="text" /></td>
         </tr>  
        </tbody> 
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_18"> 
        <thead> 
         <tr> 
          <th colspan="3">第八名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511807" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511807" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511807" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511808" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511808" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511808" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511809" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511809" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511809" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511810" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511810" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511810" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511811" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511811" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511811" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511812" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511812" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511812" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511813" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511813" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511813" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511814" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511814" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511814" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511815" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511815" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511815" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511816" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511816" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511816" class="amount"><input type="text" /></td>
         </tr>  
        </tbody>
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_19"> 
        <thead> 
         <tr> 
          <th colspan="3">第九名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_511907" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_511907" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511907" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511908" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_511908" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511908" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511909" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_511909" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511909" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511910" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_511910" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511910" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511911" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_511911" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511911" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511912" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_511912" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511912" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511913" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_511913" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511913" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511914" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_511914" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511914" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511915" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_511915" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511915" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_511916" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_511916" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_511916" class="amount"><input type="text" /></td>
         </tr>  
        </tbody>
       </table> </li> 
      <li> 
       <table class="u-table2 play_tab_20"> 
        <thead> 
         <tr> 
          <th colspan="3">第十名</th> 
         </tr> 
        </thead> 
        <tbody> 
         <tr> 
          <td id="play_name_512007" class="name"><span class="ball c-n1"></span></td> 
          <td id="play_odds_512007" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512007" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512008" class="name"><span class="ball c-n2"></span></td> 
          <td id="play_odds_512008" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512008" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512009" class="name"><span class="ball c-n3"></span></td> 
          <td id="play_odds_512009" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512009" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512010" class="name"><span class="ball c-n4"></span></td> 
          <td id="play_odds_512010" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512010" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512011" class="name"><span class="ball c-n5"></span></td> 
          <td id="play_odds_512011" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512011" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512012" class="name"><span class="ball c-n6"></span></td> 
          <td id="play_odds_512012" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512012" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512013" class="name"><span class="ball c-n7"></span></td> 
          <td id="play_odds_512013" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512013" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512014" class="name"><span class="ball c-n8"></span></td> 
          <td id="play_odds_512014" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512014" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512015" class="name"><span class="ball c-n9"></span></td> 
          <td id="play_odds_512015" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512015" class="amount"><input type="text" /></td>
         </tr> 
         <tr> 
          <td id="play_name_512016" class="name"><span class="ball c-n10"></span></td> 
          <td id="play_odds_512016" class="odds"><span class="c-txt3">--</span></td> 
          <td id="play_text_512016" class="amount"><input type="text" /></td>
         </tr>  
        </tbody> 
       </table> </li> 
     </ul>
    <div class="cont-col3-hd clearfix" style="margin-top: 20px"> 
     <!-- 
        <div class="cont-col3-box1">
         	   投注类型：
            <a href="#" class="cur">快捷</a>
            <a href="#">一般</a>
        </div>
     --> 
     <div class="cont-col3-box2"> 
<!--       <label> <input type="checkbox" class="u-cb1" /> 预设 </label>  -->
      	金额 
      <input id="bet_money2" type="text" class="bet-money" /> 
      <a href="javascript:parent.UserBet.openBetWindow()" id="openBetWinBtn2" class="u-btn1">确定</a> 
      <a href="javascript:parent.UserBet.resetData()" class="u-btn1">重置</a> 
     </div> 
    </div> 
    </div> 
   </div> 
  </div> 
					    
					    

					 <div id="game_count"> 
						 <div class="count-wrap">
					   <table class="u-table2" id="stat_qiu">
                           <thead>
                               <tr>
                                   <th class="u-tb3-th2 select" id="qiu1_th" onclick="StatGame.showStat(1)" qiu="1">冠军</th>
                                   <th class="u-tb3-th2" id="qiu2_th" onclick="StatGame.showStat(2)"  qiu="2">亚军</th>
                                   <th class="u-tb3-th2" id="qiu3_th" onclick="StatGame.showStat(3)"  qiu="3">第三名</th>
                                   <th class="u-tb3-th2" id="qiu4_th" onclick="StatGame.showStat(4)" qiu="4">第四名</th>
                                   <th class="u-tb3-th2" id="qiu5_th" onclick="StatGame.showStat(5)" qiu="5">第五名</th>
                                   <th class="u-tb3-th2" id="qiu6_th" onclick="StatGame.showStat(6)" qiu="6">第六名</th>
                                   <th class="u-tb3-th2" id="qiu7_th" onclick="StatGame.showStat(7)" qiu="7">第七名</th>
                                   <th class="u-tb3-th2" id="qiu8_th" onclick="StatGame.showStat(8)" qiu="8">第八名</th>
                                   <th class="u-tb3-th2" id="qiu9_th" onclick="StatGame.showStat(9)" qiu="9">第九名</th>
                                   <th class="u-tb3-th2" id="qiu10_th" onclick="StatGame.showStat(10)" qiu="10">第十名</th>
                               </tr>
                           </thead>
                       </table>
                       <table class="u-table4">
                           <tbody>
                               <tr>
                                   <td class="f1 fwb">1</td>
                                   <td class="f1 fwb">2</td>
                                   <td class="f1 fwb">3</td>
                                   <td class="f1 fwb">4</td>
                                   <td class="f1 fwb">5</td>
                                   <td class="f1 fwb">6</td>
                                   <td class="f1 fwb">7</td>
                                   <td class="f1 fwb">8</td>
                                   <td class="f1 fwb">9</td>
                                   <td class="f1 fwb">10</td>
                               </tr>
                                <tr id="qiu1_body" >
                                   <td id="q1_1_time">0</td>
                                   <td id="q1_2_time">0</td>
                                   <td id="q1_3_time">0</td>
                                   <td id="q1_4_time">0</td>
                                   <td id="q1_5_time">0</td>
                                   <td id="q1_6_time">0</td>
                                   <td id="q1_7_time">0</td>
                                   <td id="q1_8_time">0</td>
                                   <td id="q1_9_time">0</td>
                                   <td id="q1_10_time">0</td>
                               </tr>
                               <tr id="qiu2_body" class="hide">
                                   <td id="q2_1_time">0</td>
                                   <td id="q2_2_time">0</td>
                                   <td id="q2_3_time">0</td>
                                   <td id="q2_4_time">0</td>
                                   <td id="q2_5_time">0</td>
                                   <td id="q2_6_time">0</td>
                                   <td id="q2_7_time">0</td>
                                   <td id="q2_8_time">0</td>
                                   <td id="q2_9_time">0</td>
                                   <td id="q2_10_time">0</td>
                               </tr>
                               <tr id="qiu3_body" class="hide">
                                   <td id="q3_1_time">0</td>
                                   <td id="q3_2_time">0</td>
                                   <td id="q3_3_time">0</td>
                                   <td id="q3_4_time">0</td>
                                   <td id="q3_5_time">0</td>
                                   <td id="q3_6_time">0</td>
                                   <td id="q3_7_time">0</td>
                                   <td id="q3_8_time">0</td>
                                   <td id="q3_9_time">0</td>
                                   <td id="q3_10_time">0</td>
                               </tr>
                               <tr id="qiu4_body" class="hide">
                                   <td id="q4_1_time">0</td>
                                   <td id="q4_2_time">0</td>
                                   <td id="q4_3_time">0</td>
                                   <td id="q4_4_time">0</td>
                                   <td id="q4_5_time">0</td>
                                   <td id="q4_6_time">0</td>
                                   <td id="q4_7_time">0</td>
                                   <td id="q4_8_time">0</td>
                                   <td id="q4_9_time">0</td>
                                   <td id="q4_10_time">0</td>
                               </tr>
                               <tr id="qiu5_body" class="hide">
                                   <td id="q5_1_time">0</td>
                                   <td id="q5_2_time">0</td>
                                   <td id="q5_3_time">0</td>
                                   <td id="q5_4_time">0</td>
                                   <td id="q5_5_time">0</td>
                                   <td id="q5_6_time">0</td>
                                   <td id="q5_7_time">0</td>
                                   <td id="q5_8_time">0</td>
                                   <td id="q5_9_time">0</td>
                                   <td id="q5_10_time">0</td>
                               </tr>
                               <tr id="qiu6_body" class="hide">
                                   <td id="q6_1_time">0</td>
                                   <td id="q6_2_time">0</td>
                                   <td id="q6_3_time">0</td>
                                   <td id="q6_4_time">0</td>
                                   <td id="q6_5_time">0</td>
                                   <td id="q6_6_time">0</td>
                                   <td id="q6_7_time">0</td>
                                   <td id="q6_8_time">0</td>
                                   <td id="q6_9_time">0</td>
                                   <td id="q6_10_time">0</td>
                               </tr>
                               <tr id="qiu7_body" class="hide">
                                   <td id="q7_1_time">0</td>
                                   <td id="q7_2_time">0</td>
                                   <td id="q7_3_time">0</td>
                                   <td id="q7_4_time">0</td>
                                   <td id="q7_5_time">0</td>
                                   <td id="q7_6_time">0</td>
                                   <td id="q7_7_time">0</td>
                                   <td id="q7_8_time">0</td>
                                   <td id="q7_9_time">0</td>
                                   <td id="q7_10_time">0</td>
                               </tr>
                               <tr id="qiu8_body" class="hide">
                                   <td id="q8_1_time">0</td>
                                   <td id="q8_2_time">0</td>
                                   <td id="q8_3_time">0</td>
                                   <td id="q8_4_time">0</td>
                                   <td id="q8_5_time">0</td>
                                   <td id="q8_6_time">0</td>
                                   <td id="q8_7_time">0</td>
                                   <td id="q8_8_time">0</td>
                                   <td id="q8_9_time">0</td>
                                   <td id="q8_10_time">0</td>
                               </tr>
                               <tr id="qiu9_body" class="hide">
                                   <td id="q9_1_time">0</td>
                                   <td id="q9_2_time">0</td>
                                   <td id="q9_3_time">0</td>
                                   <td id="q9_4_time">0</td>
                                   <td id="q9_5_time">0</td>
                                   <td id="q9_6_time">0</td>
                                   <td id="q9_7_time">0</td>
                                   <td id="q9_8_time">0</td>
                                   <td id="q9_9_time">0</td>
                                   <td id="q9_10_time">0</td>
                               </tr>
                               <tr id="qiu10_body" class="hide">
                                   <td id="q10_1_time">0</td>
                                   <td id="q10_2_time">0</td>
                                   <td id="q10_3_time">0</td>
                                   <td id="q10_4_time">0</td>
                                   <td id="q10_5_time">0</td>
                                   <td id="q10_6_time">0</td>
                                   <td id="q10_7_time">0</td>
                                   <td id="q10_8_time">0</td>
                                   <td id="q10_9_time">0</td>
                                   <td id="q10_10_time">0</td>
                               </tr>
                           </tbody>
                       </table>
                       
                      <table class="u-table2 mt5">
                           <thead>
                               <tr id="stat_type">
                                   <th class="u-tb3-th2 select" id="stat_nums_btn" type="nums" onclick="StatGame.showStatType(this)">第一球</th>
                                   <th class="u-tb3-th2" id="stat_size_btn" type="size" onclick="StatGame.showStatType(this)">大小</th>
                                   <th class="u-tb3-th2" id="stat_firsts_btn" type="firsts" onclick="StatGame.showStatType(this)">单双</th>
                                   <th class="u-tb3-th2" id="stat_gyh_btn" type="gyh" onclick="StatGame.showStatType(this)">冠、亚军和</th>
                                   <th class="u-tb3-th2" id="stat_gyhdx_btn" type="gyhdx" onclick="StatGame.showStatType(this)">冠、亚军和 大小</th>
                                   <th class="u-tb3-th2" id="stat_gyhds_btn" type="gyhds" onclick="StatGame.showStatType(this)">冠、亚军和 单双</th>
                               </tr>
                           </thead>
                       </table>
                       <table class="u-table4 table-td-valign-top">
                           <tbody>
                               <tr id="qiu1_nums" class="stattd "></tr>
                               <tr id="qiu2_nums" class="stattd hide"></tr>
                               <tr id="qiu3_nums" class="stattd hide"></tr>
                               <tr id="qiu4_nums" class="stattd hide"></tr>
                               <tr id="qiu5_nums" class="stattd hide"></tr>
                               <tr id="qiu6_nums" class="stattd hide"></tr>
                               <tr id="qiu7_nums" class="stattd hide"></tr>
                               <tr id="qiu8_nums" class="stattd hide"></tr>
                               <tr id="qiu9_nums" class="stattd hide"></tr>
                               <tr id="qiu10_nums" class="stattd hide"></tr>
                               <tr id="qiu1_size" class="stattd hide"></tr>
                               <tr id="qiu2_size" class="stattd hide"></tr>
                               <tr id="qiu3_size" class="stattd hide"></tr>
                               <tr id="qiu4_size" class="stattd hide"></tr>
                               <tr id="qiu5_size" class="stattd hide"></tr>
                               <tr id="qiu6_size" class="stattd hide"></tr>
                               <tr id="qiu7_size" class="stattd hide"></tr>
                               <tr id="qiu8_size" class="stattd hide"></tr>
                               <tr id="qiu9_size" class="stattd hide"></tr>
                               <tr id="qiu10_size" class="stattd hide"></tr>
                               <tr id="qiu1_firsts" class="stattd hide"></tr>
                               <tr id="qiu2_firsts" class="stattd hide"></tr>
                               <tr id="qiu3_firsts" class="stattd hide"></tr>
                               <tr id="qiu4_firsts" class="stattd hide"></tr>
                               <tr id="qiu5_firsts" class="stattd hide"></tr>
                               <tr id="qiu6_firsts" class="stattd hide"></tr>
                               <tr id="qiu7_firsts" class="stattd hide"></tr>
                               <tr id="qiu8_firsts" class="stattd hide"></tr>
                               <tr id="qiu9_firsts" class="stattd hide"></tr>
                               <tr id="qiu10_firsts" class="stattd hide"></tr>
                               <tr id="qiu1_gyh" class="stattd hide"></tr>
                               <tr id="qiu1_gyhdx" class="stattd hide"></tr>
                               <tr id="qiu1_gyhds" class="stattd hide"></tr>
                           </tbody>
                       </table>
                  </div>

					</div> 
			</div>
			<!--/cont-main-->
			
			<div id="right_page" class="cont-sider">
	<div class="sider-box1 mt5" id="stat_play_ctn">
		<table class="u-table2">
			<thead> 
	         <tr> 
	          <th id="stat_play_list_desc">第一球</th> 
	         </tr> 
	        </thead> 
		</table>
		<table class="u-table5">
			<tbody id="stat_play_list">
			</tbody>
		</table>
	</div>
	<div id="kjtz-ctn" class="mt5 hide">
	<table class="u-table2 play_tab_85">
		<thead>
			<tr>
				<th colspan="3">快捷投注</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'DANMA_PLAYIDS')"><span>单码</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'XIAODAN_PLAYIDS')"><span>小单</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'HEDAN_PLAYIDS')"><span>合单</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'SHUANMA_PLAYIDS')"><span>双码</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'XIAOSHUAN_PLAYIDS')"><span>小双</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'HESHUAN_PLAYIDS')"><span>合双</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'DAMA_PLAYIDS')"><span>大码</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'DADAN_PLAYIDS')"><span>大单</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'HEDA_PLAYIDS')"><span>合大</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'XIAOMA_PLAYIDS')"><span>小码</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'DASHUAN_PLAYIDS')"><span>大双</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'HEXIAO_PLAYIDS')"><span>合小</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'LINTOU_PLAYIDS')"><span>0头</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LINWEI_PLAYIDS')"><span>0尾</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'WUWEI_PLAYIDS')"><span>5尾</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'YITOU_PLAYIDS')"><span>1头</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'YIWEI_PLAYIDS')"><span>1尾</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LIUWEI_PLAYIDS')"><span>6尾</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'ERTOU_PLAYIDS')"><span>2头</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'ERWEI_PLAYIDS')"><span>2尾</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'QIWEI_PLAYIDS')"><span>7尾</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'SANTOU_PLAYIDS')"><span>3头</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'SANWEI_PLAYIDS')"><span>3尾</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'BAWEI_PLAYIDS')"><span>8尾</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'SITOU_PLAYIDS')"><span>4头</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'SIWEI_PLAYIDS')"><span>4尾</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'JIU_PLAYIDS')"><span>9尾</span></td>
			</tr>
			<tr>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'鼠')"><span>鼠</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'龙')"><span>龙</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'猴')"><span>猴</span></td>
			</tr>
			<tr>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'牛')"><span>牛</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'蛇')"><span>蛇</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'鸡')"><span>鸡</span></td>
			</tr>
			<tr>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'虎')"><span>虎</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'马')"><span>马</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'狗')"><span>狗</span></td>
			</tr>
			<tr>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'兔')"><span>兔</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'羊')"><span>羊</span></td>
				<td class="sx_btns" onclick="LHC_KJTZ.sxtz(this,'猪')"><span>猪</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'HONG')"><span>红</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LAN')"><span>蓝</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LV')"><span>绿</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'HONE_DAN')"><span>红单</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LAN_DAN')"><span>蓝单</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LV_DAN')"><span>绿单</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'HONE_SHUAN')"><span>红双</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LAN_SHUAN')"><span>蓝双</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LV_SHUAN')"><span>绿双</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'HONE_DA')"><span>红大</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LAN_DA')"><span>蓝大</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LV_DA')"><span>绿大</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'HONE_XIAO')"><span>红小</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LAN_XIAO')"><span>蓝小</span></td>
				<td onclick="LHC_KJTZ.kjtz(this,'LV_XIAO')"><span>绿小</span></td>
			</tr>
			<tr>
				<td onclick="LHC_KJTZ.kjtz(this,'QUANGXUAN')"><span>全选</span></td>
				<td onclick="LHC_KJTZ.reverseSel(this,'QUANGXUAN')"><span>反选</span></td>
				<td onclick="LHC_KJTZ.cancelAll(this)"><span>取消</span></td>
			</tr>
		</tbody>
	</table>
</div>
</div>
		</div>
	</div>
	<!--/content-->
</div>
<div id="resultList" class="tuozhuai menu1 " >
	<div class="result_list_header">
		<span>弹窗可拖动</span>
		<span onclick=" breakeds(72)" style="margin-left:50px;color:red;float: left">刷新</span>
		<div class="result_list_close" onclick="$('#resultList').hide();">×</div>
		<div id="resultListHeader" style="background:Gainsboro;text-align:center;font-weight: bold;">极速赛车 - 结果走势</div>
	</div>
	<div class="result_list_block T_PK10 ">
		<table class="list table_ball" style="border-bottom: 1px solid #cdd0d4;">
			<thead><tr><th colspan="2">期数</th><th colspan="4">时间</th>
				<th id="resultHead" colspan="14">
					<div id="" class="hm" onclick="tz(this)">号码</div>
					<div id="" class="dx" onclick="tz(this)">大小</div>
					<div id="" class="ds" onclick="tz(this)">单双</div>
					<div id="" class="gy" onclick="tz(this)">冠亚/龙虎</div></th>
				</tr>
			</thead>
			<tbody id="hm" class="tzbox tzbox_on">				
			</tbody>
			<tbody id="dx" class="tzbox">			
			</tbody>		
			<tbody id="ds" class="tzbox">				
			</tbody>			
			<tbody id="gy" class="tzbox">
				
			</tbody>
		</table>
	</div>
	
	</div>
	

	
	
	<div class="kxje">
		<div class="kxbox">
			<div class="kxbox_top">
				快选金额
				<div class="colse1">X</div>
			</div>
					
			<div class="kxbox_cen">
				<div>
					<input name="bet_1" placeholder="快选金额" class="ds">
					<input name="bet_2" placeholder="快选金额" class="ds">
					<input name="bet_3" placeholder="快选金额" class="ds">
					<input name="bet_4" placeholder="快选金额" class="ds">
					<input name="bet_5" placeholder="快选金额" class="ds">
					<input name="bet_6" placeholder="快选金额" class="ds">
					<input name="bet_7" placeholder="快选金额" class="ds">
					<input name="bet_8" placeholder="快选金额" class="ds">
				</div>
			
				<label>
				    <input name="settingbet" type="radio" id="settingbet_0" value="1" checked="checked">启动
				</label>
				<label>
    				<input name="settingbet" type="radio" id="settingbet_1" value="0">停用
				</label>
				<br/>
				<br/>
				<span class="button" onclick="submitsetting()">储存</span>
			</div>
		</div>
	</div>
	


<!--/main-->

<script type="text/javascript">
window.onload = function() {
	document.onkeydown = function (e) {
		var theEvent = window.event || e;
		var code = theEvent.keyCode || theEvent.which;
		if (code == 13) {
			if(parent.UserBet.step==0) {
				parent.UserBet.openBetWindow();
			}else if(parent.UserBet.step==1){
				parent.UserBet.submitBet();
				if(parent.UserBet.step==0){
					setTimeout(function() {
						parent.layer.closeAll();
			        }, 200);
				}
			}
		}
	}
}
</script>
<script type="text/javascript" src="/static/js/lottery/pk10/stat.js" async="async"></script>
</body>
</html>
<link href="/jh/css/new.css" rel="stylesheet" type="text/css" />
<script src="/jh/js/new_new.js" type="text/javascript"></script>
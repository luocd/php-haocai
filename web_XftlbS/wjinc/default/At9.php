<?php $this->display('head.php');?>
<div class="banner-about">
    <div class="radio">
    <div class="container">
        <span>最新消息 / NEWS</span>
        <marquee id="noticeDom" scrollamount="4" scrolldelay="100" direction="left" onmouseover="this.stop();" onmouseout="this.start();">全网第一最具公信力信誉平台！两面赔率1.993  定位赔率9.93  期期返水0.5%！集齐当红最热高频一站式体验！玩法公平、规则公正、信誉公开！大额无忧！百万取款3分钟内火速到账！</marquee>
    </div>
</div>
</div>
<div class="main">
	<div class="max">
    <div class="container clearfix">
        <div class="menu">
            <ul>
                <li class="about">
                    <a href="At1.php">关于我们</a>
                </li>
                <li class="contact">
                    <a href="At9.php">联系我们</a>
                </li>
                <li class="partner">
                    <a href="At8.php">联盟合作</a>
                </li>
                <li class="deposit">
                    <a href="At3.php">存款帮助</a>
                </li>
                <li class="withdraw">
                    <a href="At7.php">提款帮助</a>
                </li>
                <li class="question">
                    <a href="At2.php">常见问题</a>
                </li>
            </ul>
        </div>
        <div class="content">
            <div class="text">
                <div class="tit">
                    联络我们 / CONTACT US
                </div>
                <div>
                    <p>好彩网的客服中心全年无休，提供1周7天、每天19小时的优质服务。</p>
                    <p class="mgb10">如果您对本网站的使用有任何疑问，可以通过下列任一方式与客服人员联系，享受最实时的服务 点击"在线客服"链接，即可进入在线客服系统与客服人员联系。 您亦可使用Email与客服人员取得联系！ </p>
                    <p class="cyl">联系方式：</p>
                    <p>联系电话：<span class="cyl">+63(939)2255555</span></p>
                    <p>正牌客服QQ：<span class="cyl">####</span></p>
                    <p>代理QQ：<span class="cyl">####</span></p>
                    <p>投诉建议：<span class="cyl">####@qq.com</span></p>
                    <p>我们也能收到您宝贵的意见(务必填写真实的Email、QQ，以便我们能及时与您取得联系！</p>
                    <p>好彩网真诚欢迎大家，并以公平、公正、公开、诚信经营的理念服务大家！</p>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<?php $this->display('foot.php');?>
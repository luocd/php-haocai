﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>.8二维码示例</title>
    <script src="jquery-1.10.2.min.js"></script>

  
  <script>
    $(document).ready(function(){
		
	    var datas = 'mer='+$('#mer').val()+'&sig='+$('#sig').val()+'&notify='+$('#notify').val()+'&num='+$('#num').val()+'&payType='+$('#payType').val()+'&settleType='+$('#settleType').val()+'&rate='+$('#rate').val()+'&certno='+$('#certno').val()+'&mobile='+$('#mobile').val()+'&accountno='+$('#accountno').val()+'&accountName='+$('#accountName').val()+'&bankno='+$('#bankno').val()+'&bankName='+$('#bankName').val()+'&bankType='+$('#bankType').val();
        $.ajax({
            type:"POST",
            url:"passivePay.php",
            data:datas,
            dataType:"json",
            success:function (response) {
                 alert(response+"---"+xhr);
                $('#merchno').html("商户号:"+response.merchno);
                $('#respCode').html("响应码:"+response.respCode);
                $('#message').html("响应消息:"+response.message);
                $('#traceno').html("商户流水号:"+response.traceno);
                $('#refno').html("渠道订单号:"+response.refno);
                // $('#QrCode').html("二维码信息:"+response.barCode);
                $('#remark').html("备注:"+response.remark);
                $("#QrCode").html(null);
                $("#QrCode").qrcode({
                    render: "table",
                    correctLevel: 0,//纠错等级
                    width: 200, //宽度
                    height:200, //高度
                    text: response.barCode //任意内容
                });
            },
            error:function (xhr,errorText,errorType) {
                // alert(errorText+':'+errorType);
                // alert("错误:"+xhr.status+":"+xhr.statusText+":"+errorText+':'+errorType);
            }
        });
		
	});
	
	

  </script>
</head>
<body>

</body>
</html>

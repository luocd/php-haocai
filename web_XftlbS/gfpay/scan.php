<!DOCTYPE html>
<html lang="zh-CN">
<head>
<title></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" id="viewport" name="viewport">
<?php
/**
 * Created by PhpStorm.
 * User: 二维码
 * Date: 2016/10/13
 * Time: 18:01
 */

date_default_timezone_set('PRC');

$code=$_REQUEST["bankid"];

if($code=="qq"){
	$payType = 8;
	$name="QQ";
	
}
else{
	
	$payType = 32;
	$name="银联钱包";
}


$settleType = 1;
$amount = $_REQUEST['amount'];
//$amount="0.1";
$merchno = "820440348160016";
$notifyUrl = "http://".$_SERVER['HTTP_HOST']."/gfpay/back.php";
$signature = "54C6CB591DC8D79B76E543BD21694C8B";


$url = 'http://api.posp168.com/passivePay';//二维码被扫接口

$fee=$amount*@$_POST['rate'];
//手续费不能低于一份
if ($fee<0.01){
    $fee = 0.01;
}
$order_no=date("His");

$post_data = array(
    "amount"=>$amount,
    'payType'=>$payType,
    'settleType'=>$settleType,
    'fee'=>$fee,
    'merchno'=>$merchno,
    'traceno'=>$order_no."_".$_REQUEST["rechargeId"],
    'notifyUrl'=>$notifyUrl,
    'certno'=>'',
    'mobile'=>'',
    'accountno'=>'',
    'accountName'=>'',
    'bankno'=>'',
    'bankName'=>'',
    'bankType'=>'',
    'goodsName'=>'pay',
    'remark'=>"remark"
);
$temp='';
ksort($post_data);//对数组进行排序
//遍历数组进行字符串的拼接
foreach ($post_data as $x=>$x_value){
    if ($x_value != null){
        $temp = $temp.$x."=".iconv('UTF-8','GBK//IGNORE',$x_value)."&";
    }
}
$md5=md5($temp.$signature);
$reveiveData = $temp.'signature'.'='.$md5;
$param=$post_data;
$param['signature'] = $md5;
$jsondata= json_encode($param);
$curl = curl_init();

$curl = curl_init();
//设置抓取的url
curl_setopt($curl, CURLOPT_URL, $url);
//设置头文件的信息作为数据流输出
curl_setopt($curl, CURLOPT_HEADER, false);
//设置获取的信息以文件流的形式返回，而不是直接输出。
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//设置post方式提交
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $reveiveData);
//执行命令
$data = curl_exec($curl);
//关闭URL请求
curl_close($curl);
//return iconv('GB2312', 'UTF-8', $data);
//显示获得的数据
$res= iconv('GBK//IGNORE', 'UTF-8', $data);
//echo $data;

$res=json_decode($res,true);
if($res["respCode"]=="00"){
	$qrcode=$res["barCode"];
	
}
else{
	
	die($res["message"]);
}


?>
<body style="text-align:center;">
<img src="http://pan.baidu.com/share/qrcode?w=150&h=150&url=<?php echo urlencode($qrcode) ?>"  width='215' height="215">
<br>
 应付金额：
                                <span style="color:red;">
                                    <?php echo $_REQUEST['amount'];?>
                                </span>
                                元
<br><br>
请使用<?php echo $name;?>扫一扫 支付

	<span id="dingdan"  ddcode="<?php echo $_REQUEST["rechargeId"];?>"  class="wx_right" ></span>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	
	 <script>	
			
		var timer;
		$(function(){
			var handler = function(){
				var orderId = $("#dingdan").attr("ddcode");
				
				$.post("query.php?orderId="+orderId,null,function(msg){
					if(msg == 'SUCCESS'){
						document.location.href="success.html";
						clearInterval(timer);
					}
				});
			}
			timer = setInterval(handler , 5000);
		});
		</script>	

	
</html>

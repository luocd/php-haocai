<?php
require_once('sqlin.php');
$conf['debug']['level']=5;

/*		数据库配置		*/
$conf['db']['dsn']='mysql:host=127.0.0.1:3306;dbname=dishini';
$conf['host']='127.0.0.1';
$conf['port']='3306';
$conf['db']['user']='root';
$conf['db']['password']='root';
$conf['db']['charset']='utf8';
$conf['db']['prename']='ssc_';

$conf['safepass']='123456';     //后台登陆安全码

$conf['cache']['expire']=0;
$conf['cache']['dir']='_cache_$98ER29@fw!d#s4fef/';
$conf['cache']['dirqiantai']='G:\\www\\php-haocai\\web_XftlbS\\_cache_$98sER9@fw!d#s4fef\\'; //C:\\Users\\Administrator\\Desktop\\dsn\\admin
$conf['cache']['dirwap']='G:\\www\\php-haocai\\wap_X1imZc\\_cache_$98sER9@fw!d#s4fef\\'; //此处设置手机站前台缓存目录,请写绝对路径
$conf['url_modal']=2;
$conf['action']['template']='wjinc/admin/';
$conf['action']['modals']='wjaction/admin/';
$conf['member']['sessionTime']=30*60;	// 用户有效时长
$conf['node']['access']='http://127.0.0.1:8860';	// node访问基本路径

error_reporting(E_ERROR & ~E_NOTICE);
ini_set('date.timezone', 'asia/shanghai');
ini_set('display_errors', 'Off');